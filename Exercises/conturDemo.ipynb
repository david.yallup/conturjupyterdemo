{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contur module interactive demo\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What's the problem?\n",
    "\n",
    "At the Large Hadron Collider vast data sets are collected. $\\mathcal{O}(10^9)$ particles are collided per second and teams of researchers put out a variety of different analyses of this data, sensitive to a variety of phenomena. Combining and making sense of this data is the job of a theorist wanting to test a new model of particle interactions. *This isn't an easy task!*\n",
    "\n",
    "Contur (Constraints on new theories using Rivet) is a toolkit designed to make this easy.\n",
    "\n",
    "### Software\n",
    "* [**Herwig**](https://herwig.hepforge.org) - encode the physics model and generate predictions *(included in this image but has no python API so not shown here)*. \n",
    "* [**Rivet**](https://rivet.hepforge.org) - analysis package to turn the generated predictions into physical observables.\n",
    "* [**YODA**](https://yoda.hepforge.org) - histogram format used by Rivet. \n",
    "* [**Contur**](https://contur.hepforge.org) - analysis package to steer Herwig and Rivet, and collate + globally analyse the outputs to create inferences on the models.\n",
    "\n",
    "In short, Contur is a meta analysis package, interacting with other codebases and analysing their outputs!\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting started, examine the inputs\n",
    "Let's look at the example yoda file included in this demo (runpoint_0004.yoda), such an input is the jumping off point for Contur"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import yoda\n",
    "yf=yoda.read(\"runpoint_0004.yoda\")\n",
    "print \"YODA file contains %s histograms\" % len(yf)\n",
    "\n",
    "##This file is a dict of \"AnalysisObjects\", effectively histograms, just to get an impression look at the first 20\n",
    "yf.keys()[0:20]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at a particular \"AnalysisObject\", a single histogram from a chosen analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h=yf[\"/CMS_2017_I1598460/d04-x01-y01\"]\n",
    "\n",
    "print \"Histo with bin edges at %s\" % h.xEdges()\n",
    "print \"Histo with fills %s\" % h.areas()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using Contur\n",
    "\n",
    "Now let's load up the Contur python module and start using this to inspect the example histogram we picked"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import contur\n",
    "\n",
    "#check this histo we chose is valid for use in contur\n",
    "print \"Is this histo valid? %s\" % contur.stats.validHisto(h.path())\n",
    "\n",
    "#now load the background models in contur\n",
    "contur.config.buildCorr=True\n",
    "contur.init_ref(yf.keys())\n",
    "\n",
    "\n",
    "#we need two special \"always present\" members of yf to do the necessary stats, let's load our test histo to be\n",
    "#dressed in histfactory\n",
    "hf=contur.histFactory(h,yf['/_XSEC'],yf['/_EVTCOUNT'])\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Signal and background models"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "fig=plt.figure()\n",
    "ax0=fig.add_subplot(1,1,1)\n",
    "ax0.scatter(hf.signal.xVals(),hf.signal.yVals(),label=\"signal\")\n",
    "ax0.scatter(hf.background.xVals(),hf.background.yVals(),label=\"background\")\n",
    "ax0.legend()\n",
    "ax0.set_yscale(\"log\")\n",
    "ax0.set_ylim(0.000001,1000)\n",
    "fig.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Relative Uncertainty"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig=plt.figure()\n",
    "ax0=fig.add_subplot(1,1,1)\n",
    "ax0.scatter(hf.background.xVals(),hf.signal.yVals()/hf.background.yVals(),label=\"Signal/Background\")\n",
    "ax0.scatter(hf.background.xVals(),(hf.background.yMaxs()-hf.background.yVals())/hf.background.yVals(),label=\"Percent uncert in background\")\n",
    "ax0.set_ylim(0.0,0.1)\n",
    "ax0.legend()\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Correlations\n",
    "\n",
    "One very important piece to know is the correlations on the background, due to the wide array of different analyses, this information can be patchy. To analyse this data we are going to build a likelihood function like the following:\n",
    "\n",
    "$$\n",
    "L(\\mu,\\vec{\\nu}) = \\prod_i \\text{Pois}(\\mu s_i + b_i +\\sum_j \\nu_j \\mid n_i) \\prod_{j,k} \\text{Gauss}(\\nu_j \\nu_k \\mid 0,\\sigma_{\\nu,(j,k)}) \\,.\n",
    "$$\n",
    "\n",
    "Depending on what we can access relating to the uncertainties on our background model ($\\vec{\\nu}$ in this notation), will depend how much information we can include in this likelihood (more info in the likelihood = better!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(hf.background.correlationMatrix())\n",
    "plt.show()\n",
    "\n",
    "print hf._nuisErrs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Notes:\n",
    "* In this case the background correlation matrix we managed to build is diagonal, and no nuisance errors (($\\vec{\\nu}$) breakdown could be found.\n",
    "* **In this case we canot combine these counting tests, so will have to pick one. There was insufficient info**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "### Looking at a different Histogram\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h2=yf[\"/ATLAS_2016_I1467454:LMODE=MU/d38-x01-y01\"]\n",
    "hf2=contur.histFactory(h2,yf['/_XSEC'],yf['/_EVTCOUNT'])\n",
    "plt.imshow(hf2.background.correlationMatrix())\n",
    "print hf2._nuisErrs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Notes:\n",
    "* In this case there was a correlation built (as seen from the plot), and a full breakdown of the uncertainties was found (nuisErrs) \n",
    "* **This is the optimal case, the full uncertainty on the background model can be incorporated into the likelihood**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Building the information into an analysis\n",
    "\n",
    "The calculation of statistical significance in govered by modular pieces called conturBuckets, this is already made by the histFactory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print hf2.conturBucket\n",
    "print hf.conturBucket"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The metric we construct from the likelihood is called CLs, which is a modified frequentist confidence level [[arxiv:9902006]](https://arxiv.org/abs/hep-ex/9902006). Certain asymptotic approximations to the likelihood are used [[arxiv:1007.1727]](https://arxiv.org/abs/1007.1727). Both these techniques are very commonly used in particle physics.\n",
    "\n",
    "In short the higher the CL we quote, the more confident we are the the model *isn't* allowed. The goal of a theorist is to find which model parameters are allowed and which aren't, so mapping CLs across a model is the key"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Under the hood a bit\n",
    "Let's reset the loglevel in the contur config and have a little peek at each one by reinitiating the CL calculation performed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "contur.config.conturLog.setLevel(logging.DEBUG)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hf.conturBucket.calcCLs()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#We can use the members this class builds to demonstrate the calculation in this simplest case,\n",
    "#a simple chi-square test\n",
    "b=hf.conturBucket\n",
    "mu_test=1.0\n",
    "delta_mu_test=(mu_test * b._s + b._bg ) - b._n\n",
    "import numpy as np\n",
    "import scipy.stats as sp\n",
    "\n",
    "sp.norm.sf(np.sqrt(delta_mu_test**2 * np.diag(b.cov)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hf2.conturBucket.calcCLs()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Notes\n",
    "* In the former case we noted we couldn't combine the tests so each bin is a separate counting test and the likelihood comes from one bin.\n",
    "* We used a simple background model approximation allowing an approximation of the likelihood as a chi-square\n",
    "* In the later case, we could fit all bins together, (although we're using a trivial background model so the fitting with scipy taking into account the error breakdown is essentially trivial)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Combining the blocks\n",
    "\n",
    "These blocks can be combined using a wrapper object that is then a bucket of buckets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cbb=contur.conturBucketBucket()\n",
    "cbb.addTS(hf.conturBucket)\n",
    "cbb.addTS(hf2.conturBucket)\n",
    "cbb.calcCLs()\n",
    "print cbb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is maybe a slightly boring example! The bucket from hf was already overwhelming excluding this scenario and \n",
    "adding the second doesn't change the conclusion, as expected!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Automation, automation, automation!\n",
    "\n",
    "Let's accelerate and look at two higher level steering classes, \n",
    "* yodaFactory  - wraps up all the function we just picked apart\n",
    "* conturDepot - the analysis class, controls the higher level interaction such as sampling parameter spaces"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### YodaFactory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "contur.config.conturLog.setLevel(logging.WARNING)\n",
    "\n",
    "#to make things quicker lets only use a subset of analyses\n",
    "contur.config.onlyAnalyses=[\"ATLAS_2016\"]\n",
    "contur.config.REFLOADED=False\n",
    "contur.config.buildCorr=False\n",
    "\n",
    "yf=contur.yodaFactory(\"runpoint_0004.yoda\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "yf.sortBuckets()\n",
    "yf.buildFinal()\n",
    "yf.sortedBuckets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print yf.conturPoint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ConturDepot\n",
    "\n",
    "The final stage is to relate back to the parameters from the physics model we sampled to generate the histograms we've seen, as the generator chain lives outside of python we'll just show a toy analysis of the outputs here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ls exampleGrid/*/*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cat exampleGrid/8TeV/0000/params.dat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The steering tool created a structured directories with yoda files like those we were already querying, sampled at different values for the models parameters. Those parameters are stored in these dat files for each sampled point"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ctdp=contur.conturDepot()\n",
    "print ctdp\n",
    "\n",
    "yodapaths=[]\n",
    "parampaths=[]\n",
    "import os\n",
    "from contur.scanning.os_functions import read_param_dat\n",
    "\n",
    "contur.config.onlyAnalyses=[\"ATLAS_2016\"]\n",
    "contur.config.REFLOADED=False\n",
    "contur.config.gridMode=True\n",
    "\n",
    "for root, dirs, files in os.walk(\"exampleGrid/8TeV\"):\n",
    "    for file_name in files:\n",
    "        if \"yoda\" in file_name:\n",
    "            yoda_file_path = os.path.join(root, file_name)\n",
    "            param_file_path = os.path.join(root, 'params.dat')\n",
    "            yodapaths.append(yoda_file_path)\n",
    "            parampaths.append(read_param_dat(param_file_path))\n",
    "contur.scanning\n",
    "for x,y in zip(yodapaths,parampaths):\n",
    "    ctdp.addParamPoint(yodafile=x,param_dict=y)\n",
    "    \n",
    "print ctdp\n",
    "ctdp.buildMapAxis()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ctdp.conturDepotInbox\n",
    "for x in ctdp.conturDepotInbox:\n",
    "    print x.yodaFactory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture\n",
    "pb=contur.plotting.conturPlotBase(ctdp, \".\")\n",
    "pb.doSavePlotfile=True\n",
    "#pb.doPlotPools=False\n",
    "pb.buildAxesfromGrid(xarg=\"Xm\", yarg=\"Y1\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pb.plotObjects\n",
    "pb.plotObjects[\"combined\"][0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Notes:\n",
    "\n",
    "* color bar has gone missing, but lighter => CLs->1\n",
    "* Other visualisation options etc. wrapped up in executables\n",
    "* From here we have a map of our sampled parameters, this opens up possibility to interact between conturDepot <-> Physics Generator. Techniques involving adaptaive sampling to cover higher dimension parameter spaces are underway\n",
    "* More advanced background modelling and more ambitious cross analysis fitting is another interesting point\n",
    "* That's before mentioning the actual physics going on!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
